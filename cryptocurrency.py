from datetime import datetime
from json import loads
from urllib.request import (
    Request,
    urlopen
)


def request(url):
    """Request uri and return a JSON object."""
    request = Request(url, headers={'User-agent': 'Mozilla/5.0'})
    with urlopen(request) as r:
        return loads(r.read())

def do_quote(self, asset=None):
    if asset.lower() in ['btc', 'xrp', 'ltc']:
        res = request(f'https://www.mercadobitcoin.net/api/{asset}/ticker/').get('ticker')
        res['date'] = datetime.fromtimestamp(res['date']).strftime('%d/%m/%Y %H:%M')
        header = ' | '.join([i.center(16, " ") for i in res.keys()])
        values = ' | '.join([str(i).center(16, " ") for i in res.values()])
        limiter = len(values) * '-'
        print(f'{header}\n{limiter}\n{values}')

def help_quote(self):
        print('\n'.join([
            'quote <coin>',
            'Return current quote of crypto assets.',
            'Assets: BTC, XRP and LTC'
        ]))

