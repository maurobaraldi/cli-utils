#class MainClass:
#	def __init__(self):
#		self.version = '0.0.1'


def print_hello(self, name=""):
	print(f'Hello {name}.')

def help_hello(self):
        print('\n'.join([
            'hello [person]',
            'Hello to the named person',
        ]))

#setattr(MainClass, 'hello', print_hello)