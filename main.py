import cmd

from tricks import (
    print_hello,
    help_hello
)
from cryptocurrency import (
    do_quote,
    help_quote
)

def register(method, name, method_args=None, method_kwargs=None):
    if name.startswith('help_'):
        pass
    elif not name.startswith('do_'):
        name = f'do_{name}'

    setattr(HelloWorld, name, method)


class HelloWorld(cmd.Cmd):
    """Simple command processor example."""

    def do_greet(self, line):
        print("hello")

    def do_EOF(self, line):
        print('\nBye...\n')
        return True

if __name__ == '__main__':
    register(print_hello, 'hello')
    register(help_hello, 'help_hello')

    register(do_quote, 'quote')
    register(help_quote, 'help_quote')

    HelloWorld().cmdloop()
